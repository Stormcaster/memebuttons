package com.stormcaster.memebutton;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.stormcaster.memebutton.Model.HoldClickListener;
import com.stormcaster.memebutton.Model.Sound;
import com.stormcaster.memebutton.Model.SoundContainer;

import org.w3c.dom.Text;

import java.io.IOException;

public class ButtonActivity extends AppCompatActivity {

    ImageButton button;
    MediaPlayer mp;
    TextView buttonName;
    SoundContainer sc = SoundContainer.getInstance();
    int soundID;
    boolean playing = false;
    Sound thisSound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
        button = (ImageButton)findViewById(R.id.button);
        soundID = this.getIntent().getIntExtra("soundID", -1);
        thisSound = sc.findSoundByID(soundID);
        buttonName = (TextView)findViewById(R.id.buttonName);
        initComponents();
    }

    private void initComponents() {
        buttonName.setText(thisSound.getName());
        if(thisSound.isSample())mp = MediaPlayer.create(this, thisSound.getSound());
        else {
            mp = MediaPlayer.create(this, Uri.parse(thisSound.getFilepath()));
        }

        button.setOnTouchListener(new HoldClickListener(ButtonActivity.this) {
            public void onTap() {
                if(!mp.isPlaying()) {
                    mp.setLooping(false);
                    mp.seekTo(0);
                    mp.start();
                }
                else mp.pause();
            }
            public void onHold() {

            }
            public void onHoldUp() {
                mp.seekTo(0);
                mp.setLooping(true);
                mp.start();

            }
        });

//        button.setOnTouchListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//
//                    case MotionEvent.ACTION_DOWN:
//                        mp.seekTo(0);
//                            mp.setLooping(true);
//                            mp.start();
//                        break;
//                    case MotionEvent.ACTION_UP:
//
//                        mp.pause();
//                        break;
//
//                }
//
//                return true;
//            }
//        });
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mp.start();
//            }
//        });
    }

    @Override
    protected void onStop() {
        mp.release();
        super.onStop();
    }
}
