package com.stormcaster.memebutton.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.stormcaster.memebutton.R;

import java.util.List;

/**
 * Created by zBlizzarDz on 04-Jun-16.
 */
public class SoundAdapter extends ArrayAdapter<Sound> {
    SoundContainer sc = SoundContainer.getInstance();
    public SoundAdapter(Context context, int resource, List<Sound> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.soundlist_item, null);
        }
        TextView soundName = (TextView)v.findViewById(R.id.soundNameTxt);
        Sound thisSound = getItem(position);
        soundName.setText(thisSound.getName());

        return v;
    }

//    @Override
//    public Sound getItem(int position) {
//        return sc.getSoundList().get(position);
//    }

}
