package com.stormcaster.memebutton.Model;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by zBlizzarDz on 05-Jun-16.
 */
public class HoldClickListener implements View.OnTouchListener {

    private final GestureDetector gestureDetector;

    public HoldClickListener(Context ctx){

        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {

            onTap();
            return super.onSingleTapUp(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            onHoldUp();
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            onHold();
        }



    }
    public void onTap() {

    }
    public void onHold() {

    }
    public void onHoldUp() {

    }

}
