package com.stormcaster.memebutton.Model;

/**
 * Created by zBlizzarDz on 04-Jun-16.
 */
public class Sound {
    private int sound, id;
    private static int nextId = 0;
    private boolean isSample;
    private String name, filepath;

    public Sound(int sound, String name) {
        this.sound = sound;
        this.name = name;
        id = nextId;
        nextId++;
        filepath = "";
        isSample = true;
    }
    public Sound(String filepath, String name) {
        this.filepath = filepath;
        this.name = name;
        id = nextId;
        nextId++;
        isSample = false;
    }

    public int getId() {
        return id;
    }

    public int getSound() {
        return sound;
    }

    public void setSound(int sound) {
        this.sound = sound;
    }

    public boolean isSample() {
        return isSample;
    }

    public String getFilepath() {
        return filepath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + "\n";
    }
}
