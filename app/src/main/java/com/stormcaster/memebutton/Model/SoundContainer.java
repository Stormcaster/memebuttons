package com.stormcaster.memebutton.Model;

import com.stormcaster.memebutton.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zBlizzarDz on 04-Jun-16.
 */
public class SoundContainer {
    private static SoundContainer instance = null;
    private List<Sound> soundList;

    private SoundContainer() {
        soundList = new ArrayList<Sound>();
        addSound(new Sound(R.raw.ihateyou_th, "I HATE YOU (TH)"));
        addSound(new Sound(R.raw.ihateyou, "I HATE YOU"));
        addSound(new Sound(R.raw.first_blood, "First Blood"));

    }
    public static synchronized SoundContainer getInstance() {
        if(instance == null) instance = new SoundContainer();
        return instance;
    }
    public void addSound(Sound sound) {
        soundList.add(sound);
    }
    public List<Sound> getSoundList() {
        return soundList;
    }
    public Sound findSoundByID(int id) {
        Sound thisSound = null;
        for(Sound s : soundList) {
            if(s.getId() == id) thisSound = s;
        }
        return thisSound;
    }
    public void deleteSound(int id) {
        soundList.remove(findSoundByID(id));
    }

    public void renameSound(String newName, int id) {
        findSoundByID(id).setName(newName);
    }

}
