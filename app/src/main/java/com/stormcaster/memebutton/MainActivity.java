package com.stormcaster.memebutton;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.stormcaster.memebutton.Model.Sound;
import com.stormcaster.memebutton.Model.SoundAdapter;
import com.stormcaster.memebutton.Model.SoundContainer;

public class MainActivity extends AppCompatActivity {

    SoundContainer sc = SoundContainer.getInstance();
    ListView soundLib;
    EditText searchTxt;
    SoundAdapter sa;
    Button addBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        soundLib = (ListView)findViewById(R.id.soundListView);
        searchTxt = (EditText)findViewById(R.id.searchEditText);
        addBtn = (Button)findViewById(R.id.addSoundBtn);
        sa = new SoundAdapter(this,R.layout.soundlist_item,sc.getSoundList());
        soundLib.setAdapter(sa);
        soundLib.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ButtonActivity.class);
                Sound selected = (Sound)parent.getAdapter().getItem(position);
                intent.putExtra("soundID", selected.getId());
                startActivity(intent);
            }
        });
        soundLib.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, EditButtonActivity.class);
                Sound selected = (Sound)parent.getAdapter().getItem(position);
                if(selected.isSample()) Toast.makeText(view.getContext(),"You cannot edit sample buttons",Toast.LENGTH_SHORT).show();
                else {
                    intent.putExtra("soundID", selected.getId());
                    startActivityForResult(intent, 0);
                }
                return true;
            }
        });
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sa.getFilter().filter(s);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddButtonActivity.class);
                startActivityForResult(intent, 1);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            sa.notifyDataSetChanged();
            Log.e("TEST", "Notified");
        }
    }
}
