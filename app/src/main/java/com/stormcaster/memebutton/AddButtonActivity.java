package com.stormcaster.memebutton;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.stormcaster.memebutton.Model.Sound;
import com.stormcaster.memebutton.Model.SoundContainer;

import java.io.File;

public class AddButtonActivity extends AppCompatActivity {

    EditText filePathTxt, nameTxt;
    Button browseBtn, addBtn;
    String filePath;
    SoundContainer sc = SoundContainer.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_button);
        filePathTxt = (EditText)findViewById(R.id.filePathTxt);
        nameTxt = (EditText)findViewById(R.id.nameTxt);
        browseBtn = (Button)findViewById(R.id.browseBtn);
        addBtn = (Button)findViewById(R.id.addBtn);
        initComponents();
    }

    private void initComponents() {
        browseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FileChooser(AddButtonActivity.this).setFileListener(new FileChooser.FileSelectedListener() {
                    @Override
                    public void fileSelected(File file) {
                        filePath = file.getAbsolutePath();
                        filePathTxt.setText(filePath);
                    }
                }).showDialog();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nameTxt.getText().toString().equals("")) Toast.makeText(v.getContext(),"Please enter the name",Toast.LENGTH_SHORT).show();
                else if(filePathTxt.getText().toString().equals("")) Toast.makeText(v.getContext(), "Please select a file",Toast.LENGTH_SHORT).show();
                else {
                    sc.addSound(new Sound(filePath,nameTxt.getText().toString()));
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }

            }
        });
    }

}
