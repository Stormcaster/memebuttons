package com.stormcaster.memebutton;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.stormcaster.memebutton.Model.Sound;
import com.stormcaster.memebutton.Model.SoundContainer;

public class EditButtonActivity extends AppCompatActivity {

    EditText newNameTxt;
    Button saveBtn, deleteBtn;
    int soundID;
    Sound thisSound;
    SoundContainer sc = SoundContainer.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_button);
        soundID = this.getIntent().getIntExtra("soundID", -1);
        thisSound = sc.findSoundByID(soundID);
        newNameTxt = (EditText)findViewById(R.id.newNameTxt);
        saveBtn = (Button)findViewById(R.id.saveBtn);
        deleteBtn = (Button)findViewById(R.id.deleteBtn);
        initComponents();
    }

    private void initComponents() {
        newNameTxt.setText(thisSound.getName());

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = newNameTxt.getText().toString();
                if(newName.equals("")) {
                    Toast.makeText(v.getContext(),"Name cannot be empty",Toast.LENGTH_SHORT).show();
                }
                else {
                    sc.renameSound(newName, soundID);
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sc.deleteSound(soundID);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
